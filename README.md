# TinySh
A command shell for the Thumby console.

You can issue commands to affect your Thumby's file system, for example to copy of delete a file.

**Warning!** Use TinySh at your own risk. There may be bugs that cause data loss. But this application is intended to allow you to delete files or replace their content. With great power comes great responsibility. If you want to try things out, the Thumby emulator is good because no real files are affected.

## Status
This project is at beta stage. As far as I know, everything works. If you find something that doesn't, please let me know.

## Output
Every command produces some output. When a command completes, the output is displayed. Use the D-pad to scroll, or press A to continue.

Some commands implicitly use the output of the previous command. For example, the the `grep` command always selects lines from the previous output.

## Input
Entering text on a Thumby is laborious, so TinySh uses menus wherever possible to compose commands.

### Menu selection
When a directory appears in a menu, you might want to select it, or you might want to open it to select something inside.
* Press Right to open a directory and see its contents
* Press A to select a directory

### Keyboard usage
A keyboard only appears when absolutely necessary. If you've used TinyEdit it will be familiar. It fills most of the screen, and the line being edited is at the bottom. There's a text cursor to show where typed characters will appear.

Use the D-pad to select a key on the keyboard, then press A to type it.

To move the cursor, type the left and right arrow keys on the bottom row of the keyboard.

The keyboard has several layers. Type the shift key (bottom left of the keyboard) to cycle between them.

There are delete buttons for a character and a chunk on each keyboard layer. "Chunk" means a sequence of letters, or a sequence of non-letter characters.

One keyboard layer has a whole row of delete buttons that delete in either direction, and to either end of a line.

When you've finished entering text, press B. There's no cancel! If you need to abort a command, just switch off the Thumby.

## Commands

### run
Execute a script

Any file can be executed, but by convention script names end `.tsh`

### changedir

### pwd

### listdir

### show
Display the content of a file

You can't directly use a file's contents as the input to another command. Instead, use this command, then the command that needs the input.

### edit
If you have TinyEdit v2 installed you can edit a file. If not, this command will fail.

### copy

### move

### delete
There is no undelete, so be careful!

### makedir
The new directory is a child of the current directory

### deletedir
This will fail unless the directory is empty.

### env
Set, unset or display environment variables

Some default variables are available. The defaults have upper case names. You can use any variable name you like, but by convention, user-created variable names are camelCase (capital letters showing where word breaks would be). Numeric variable names are used for script parameters.

### echo
Variables are substituted if inside {curlies}. This is mostly useful in a script.

### grep
Search the output of the last command

The regex dialect is described in the [Micropython docs](https://docs.micropython.org/en/latest/library/re.html).
Note the "Not supported" section - it includes {curlies}, which is handy because they might get replaced by environment variables

### find
Search for file system entries

The wild cards are `*` which matches any string (including an empty string) and `?` which matches exactly one character

### >
Write the output of the last command to a file

### >>
Append the output of the last command to the end of file

### version
Show the current version of TinySh

### exit
Close TinySh and go back to the Thumby menu

## Scripting
Shell commands can be put into a script and called with the `run` command. Any file name can be used, but by convention script file names end with `.tsh`.

**Warning** There may be bugs that cause data loss. But scripts are intended to allow you to delete files or replace their content. With great power comes great responsibility. If you want to try scripting things, the Thumby emulator is good because no real files are affected.

If one of the commands in a script fails, the whole script stops. Or to put it another way, a script only completes if all its commands complete.

The output of a script is the output of the last command executed in the script. During execution of a command in the script, the output of the previous command is available in the same way as it is when running commands interactively.

The script language is very simple. But you can use it to automate tasks - for example, backing up a game save file is a good idea:
```
copy /Saves/TinyTunes/persistent.json /Saves/TinyTunes/persistent.json.bak
```

Suppose you had saved a copy of a game's save file at a point where you wanted to re-start the game. This script replaces a game's save state with a known state, and displays that state:
```
copy /Saves/Tinymine/level1.bak /Saves/Tinymine/persistent.json
show /Saves/Tinymine/persistent.json
```

When you have executed commands interactively they are recorded as `/Games/TinySh/log`. You can use the content of the log as the basis for a script. Either copy it with TinySh (but this must be during the same session because it is overwritten every time TinySh starts) or exit TinySh and save the log under another name with another program such as TinyEdit or the Thumby code editor.

### Script language
The scripting language is what is logged when you use commands interactively through the meus. Generally speaking, the command name is the same as you see on the menu. File names work as you might expect. Options default to `--long-style` but other formats are accepted. For example, all of these are equivalent:
```
listdir --detail
listdir -detail
listdir -d
listdir /detail
listdir /d
```
You can't combine multiple options. For example `foo -ab` isn't the same as `foo -a -b`.

Most commands have aliases for the benefit of people who forget which platform they're on. For example, all of these are equivalent:
```
listdir
ls
dir
list
```

### Script examples
There are test scripts - `/Games/TinySh/*.tsh` - that exercise some aspects of the scripting language. Looking at these may help with how to use the language. The script `/Games/TinySh/allTests.tsh` will run a suite of tests for many aspects of the scripting language. If everything's working as expected, it will output `All OK`.

### Environment variables in scripts
The name of the script being run is available as `{0}` and any further parameters are available as `{1}` and so on. A script gets a copy of the environment, meaning that if you set a variable during the processing of a script then it is no longer set after the script exists. Similarly, if a script calls itself recursively, environment variable assignments from one invocation of the script do not overwrite those of another.

### Piping input
If you use a pipe character - `|` - in a script, it is replaced with the output of the last command. Here's a way to remember the current directory and go back to it later
```
pwd
set oldDir |
changedir somewhere
# do stuff here
changedir {oldDir}
```

### jump (script-only command)
Commands in a script are executed sequentially unless you jump to a label. In this example, `command2` is ignored, but the others are executed.
```
command1
jump :myLabel
command2
:myLabel
command3
```
Goto may be considered harmful, but jumps are fine. `8~)`

### if (script-only command)
You can conditionally execute one command in a script with and `if` statement. By combining this with jumps, you can emulate structured programming (or write a plate of spaghetti if you prefer).

There are three forms of `if`

#### File test
This checks whether a file (or directory) exists.
```
# Make sure a Sonic dir has been created
if exists /Games/Sonic
jump :foundSonic
makedir /Games/Sonic
:foundSonic
echo Sonic dir exists
```

#### Emptiness test
This checks whether the previous command produced any output.
```
# Let's see if we have any Mario games
find --type f *.py
grep -i Mario

# No Mario means no output
if empty
echo Mario not found
```

#### Equality test
This checks whether two strings are equal.
```
# This test will fail
if cat = dog
echo Shurely shome mishtake?!

# Let's do some setup
set animal dog

# This test will pass
if {animal} = dog
echo No mistake this time.
```

#### Negation
Each type of check can be reversed with `if not`

#### Gotcha
Don't put a comment immediately after the `if`.
```
if exists somefile
# Yay, it exists
doThing
```
This example looks conditional but in fact `doThing` is executed unconditionally.

### each (script-only command)
You can repeat a command with `each`. The command is executed for every line of the output of the previous command. That output is available as `|`.

For example, one way to back up all the files in a directory would be this:
```
find --type f
grep ^/Saves/TinyTunes/[^/]+$
each
copy | /Backup
```

### choose (script-only command)
You can offer a list of choices to the user. For example, a choice of game directories might be done like this:
```
find --type d
grep ^/Games/[^/]+/$
choose
set gameDir |
# Now do something with {gameDir}...
```

## Calling other programs
Command shells on other platforms can execute other programs and get back control afterwards. This is not implemented here.

Calling something else directly is possible: that's how the file editing works with TinyEdit. But the called program has to be understood by TinySh.
